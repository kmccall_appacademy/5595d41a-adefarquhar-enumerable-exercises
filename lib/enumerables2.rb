require 'byebug'

# EASY

# Define a method that returns the sum of all the elements in its argument (an
# array of numbers).
def array_sum(arr)
  arr.reduce(0, :+)
end

# Define a method that returns a boolean indicating whether substring is a
# substring of each string in the long_strings array.
# Hint: you may want a sub_tring? helper method
def in_all_strings?(long_strings, substring)
  long_strings.all? { |string| string.include?(substring)  }
end

# Define a method that accepts a string of lower case words (no punctuation) and
# returns an array of letters that occur more than once, sorted alphabetically.
def non_unique_letters(string)
  dups = string.chars.select { |el| string.count(el) > 1 && el != ' ' }
  dups.uniq
end

# Define a method that returns an array of the longest two words (in order) in
# the method's argument. Ignore punctuation!
def longest_two_words(string)
  words = string.split
  words_length = words.sort_by { |word| word.length }
  two_longest = words_length.last(2)
end

# MEDIUM

# Define a method that takes a string of lower-case letters and returns an array
# of all the letters that do not occur in the method's argument.
def missing_letters(string)
  ('a'..'z').to_a.select { |el| string.include?(el) == false }
end

# Define a method that accepts two years and returns an array of the years
# within that range (inclusive) that have no repeated digits. Hint: helper
# method?
def no_repeat_years(first_yr, last_yr)
  years = first_yr..last_yr
  years.select { |yr| yr.to_s.chars == yr.to_s.chars.uniq }
end

def not_repeat_year?(year)
end

# HARD

# Define a method that, given an array of songs at the top of the charts,
# returns the songs that only stayed on the chart for a week at a time. Each
# element corresponds to a song at the top of the charts for one particular
# week. Songs CAN reappear on the chart, but if they don't appear in consecutive
# weeks, they're "one-week wonders." Suggested strategy: find the songs that
# appear multiple times in a row and remove them. You may wish to write a helper
# method no_repeats?
def one_week_wonders(songs)
  songs.select { |song| no_repeats?(song, songs) }.uniq
end

def no_repeats?(song_name, songs)
  songs.each_index { |idx| return false if songs[idx, 2].count(song_name) > 1 }
  true
end

# Define a method that, given a string of words, returns the word that has the
# letter "c" closest to the end of it. If there's a tie, return the earlier
# word. Ignore punctuation. If there's no "c", return an empty string. You may
# wish to write the helper methods c_distance and remove_punctuation.

def for_cs_sake(string)
  words = string.split.map! { |word| word.delete("^a-z") }
  return '' if words.none? { |word| word.include?('c') }

  closest = ''
  words.each do |word|
    next unless word.include?('c')
    if word.include?('c') && closest == ''
      closest = word
    elsif c_distance(word) < c_distance(closest)
      closest = word
    end
  end
  closest
end

def c_distance(word)
  c_idx = word.rindex('c')
  distance = word.length - c_idx
end

# Define a method that, given an array of numbers, returns a nested array of
# two-element arrays that each contain the start and end indices of whenever a
# number appears multiple times in a row. repeated_number_ranges([1, 1, 2]) =>
# [[0, 1]] repeated_number_ranges([1, 2, 3, 3, 4, 4, 4]) => [[2, 3], [4, 6]]

def repeated_number_ranges(arr)
  ranges = []
  arr.each_with_index do |num, idx|
    next if arr[idx] == arr[idx - 1]
    ranges << range(arr, num, idx) if arr.count(num) > 1
  end

  ranges
end

def range(arr, num, idx)
  idxs = []
  arr.each_with_index do |r_num, r_idx|
    next if r_idx < idx
    idxs << r_idx if r_num == num
    break if r_num != idxs[0] && idxs.length > 1
  end

  [idxs.first, idxs.last]
end
